import unittest

import dialogflow


class DialogflowTestCase(unittest.TestCase):

    def setUp(self):
        self.app = dialogflow.app.test_client()

    def test_index(self):
        rv = self.app.get('/')
        self.assertIn('Welcome to Qiscus DialogFlow', rv.data.decode())


if __name__ == '__main__':
    unittest.main()
